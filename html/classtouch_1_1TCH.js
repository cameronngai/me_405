var classtouch_1_1TCH =
[
    [ "__init__", "classtouch_1_1TCH.html#a772f8c265a626764d78cf92bf0a1c642", null ],
    [ "collect", "classtouch_1_1TCH.html#a3a00b2206cec3e1845037a7cfe0877cd", null ],
    [ "X", "classtouch_1_1TCH.html#ae33542992f156a575c8f71d86e87a745", null ],
    [ "Y", "classtouch_1_1TCH.html#a4de65670f873be5eb376f4adc347af1a", null ],
    [ "Z", "classtouch_1_1TCH.html#a3e3d8e72b7dee552332661406912aca8", null ],
    [ "cx", "classtouch_1_1TCH.html#a8660a103a19881fb1b25ae3d7e3aeeb0", null ],
    [ "cy", "classtouch_1_1TCH.html#ae82403f8cb12bb302e3a359ca6e3c8e2", null ],
    [ "Lx", "classtouch_1_1TCH.html#ae755ee86a0ae0b5d39d910508186f25e", null ],
    [ "Ly", "classtouch_1_1TCH.html#acca16b1065b863797fe38a877c764e4f", null ],
    [ "sample", "classtouch_1_1TCH.html#ae30154cd1855199babb99eb8b5997d54", null ],
    [ "sample_set", "classtouch_1_1TCH.html#a1f15669b4e367b5c289f6e593ea34d2a", null ],
    [ "xm", "classtouch_1_1TCH.html#a55f870c71cca00a9674b5e4a4ebe1269", null ],
    [ "xp", "classtouch_1_1TCH.html#aef4b549fb7dbc633ebef400479f3b29f", null ],
    [ "ym", "classtouch_1_1TCH.html#a10ae485f0ccfb2884c961b36ff88665c", null ],
    [ "yp", "classtouch_1_1TCH.html#a7e67280a31469becabce08250771885c", null ]
];