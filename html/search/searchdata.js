var indexSectionsWithContent =
{
  0: "_abcdefgiklmnoprstuvwxyz",
  1: "bekmnrtuv",
  2: "aelmntu",
  3: "aelmnstu",
  4: "_abcdefgknorstuxyz",
  5: "abcdegiklmnoprstuvwxyz"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables"
};

