var classMCP9808_1_1MCP9808 =
[
    [ "__init__", "classMCP9808_1_1MCP9808.html#a6664e2398c3da99e2a984848d810b02b", null ],
    [ "celcius", "classMCP9808_1_1MCP9808.html#a78ef58bb0a32d53882f7b1e7e3f5d2c7", null ],
    [ "check", "classMCP9808_1_1MCP9808.html#a85e4a979a45f5a21dcadf199fa16df9a", null ],
    [ "fahrenheit", "classMCP9808_1_1MCP9808.html#aa4f5c4d4b6ee9874f5395c5e6dd5fdc8", null ],
    [ "ADDRESS", "classMCP9808_1_1MCP9808.html#ab3cebb22ca2e75244eefe62472379933", null ],
    [ "i2c", "classMCP9808_1_1MCP9808.html#a683215c3bfc5564518c4b5348adad0ac", null ],
    [ "temp_bytearray", "classMCP9808_1_1MCP9808.html#ac89f11fd7fc599d58f7ffd6a442952a5", null ]
];