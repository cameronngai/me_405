# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 12:36:18 2021

@author: camer
"""

import numpy as np
from scipy.integrate import odeint
import matplotlib as mpl
import matplotlib.pyplot as plt
from math import sin
from math import cos
from math import sqrt
from math import pi
import os

import time

# Parameters

rm = 0.06 #m
lr = 0.05 #m
rb = 0.0105 #m
rg = 0.042 #m
lp = 0.110 #m
rp = 0.0325 #m
rc = 0.05 #kg
mb = 0.03 #kg
mp = 0.400 #kg
Ip = 1.88*10**(-3)+mp*rg**2 #kgm^2
b = 0.010 #Nms/rad

# Natural constants
g = 9.81 #m/s^2


# Mass inverse
def Minv(q):
    x = q[0]
    
    M11 = 7/5*mb
    M12 = (rc+7/5*rb)*mb
    M22 = ((rc+rb)**2+x**2+2/5*rb**2)*mb+Ip
    M = np.matrix([[M11,M12],[M12,M22]])
    return np.linalg.inv(M)
    


# Linear Model

# Stiffness matrix
Apred = np.matrix([[0,mb*g,0,0], 
      [mb*g,g*(mb*(rc+rb)+mp*rg),0,-b]])

# Stiffness matrix multlied to mass matrix
Ared = np.matmul(Minv([0,0]),Apred)

## Vertical lift of stiffness matrix multlied to mass matrix
A = np.append([[0,0,1,0],
              [0,0,0,1]], Ared,axis = 0)

# Response matrix
Bpred = [[0],[-lp/rm]]

# Response matrix multlied to mass matrix
Bred = np.matmul(Minv([0,0]),Bpred)

## Vertical lift of response matrix multlied to mass matrix
B = np.append([[0],[0]],Bred,axis = 0)
               

# Nonlinear Model

def R(q,Tx):
    x = q[0]
    ph = q[1]
    xdot = q[2]
    phdot = q[3]
    
    if lr**2-(rc*sin(ph)+lp*(cos(ph)-1))**2 <= 0:
        Q = 0
    else:
        Q = (rc*sin(ph)+lp*(cos(ph)-1))*(rc*cos(ph)-lp*sin(ph))/sqrt(lr**2-(rc*sin(ph)+lp*(cos(ph)-1))**2)
    B = (-lp*cos(ph)+rc*sin(ph)+Q)/(rm*cos(ph))
    
    r = np.matrix([mb*x*phdot**2+mb*g*sin(ph),
                   -2*mb*x*xdot*phdot+g*(mb*((rc+rb)*sin(ph)+x*cos(ph))+mp*rg*sin(ph))])
    r = r + np.matrix([0,B*Tx-b*phdot])
    
    return np.transpose(r)



def simulate(q0,tf,T,sample,foldername,nonlinear = False,linear = True,prestart = 0):
    
    mpl.use('AGG')
    
    # Convert units of initial conditions from (cm,deg) to (m,rad)
    q0[0] = q0[0]/100
    q0[1] = q0[1]*pi/180
    q0[2] = q0[2]/100
    q0[3] = q0[3]*pi/180
    
    tf += prestart
    
    # Create folder if not present
    if not os.path.exists(foldername):
        os.mkdir(foldername)
    
    # Linear simulation
    if linear:
        
        # Linear model
        def lin(q,t):
            Q = np.matrix(q)
            Q = Q.transpose()
            Qdot = np.matmul(A,Q)+np.matmul(B,np.matrix(T(q,t)))
            Qdot = Qdot.transpose()
            Qdot = Qdot.tolist()
            out = Qdot[0]
            return out
        
        # Simulate
        t1 = np.linspace(0,tf,sample)
        q1 = odeint(lin,q0,t1)
        
        # phase time of the simulation
        for i in range(len(t1)):
            if t1[i] >= prestart:
                j=i
                break
        t1 = t1[j:]
        t1 = [i-prestart for i in t1]
        
        # Convert back to (cm,deg)
        x1 = q1[j:,0]*100
        ph1 = q1[j:,1]*180/pi
        xdot1 = q1[j:,2]*100
        phdot1 = q1[j:,3]*180/pi
            
        # Plots
        plt.figure(1)
        plt.plot(t1,x1,'r-',linewidth=2,label='Linear')
        plt.xlim([0,max(t1)])
        
        plt.figure(2)
        plt.plot(t1,ph1,'r-',linewidth=2,label='Linear')
        plt.xlim([0,max(t1)])
        
        plt.figure(3)
        plt.plot(t1,xdot1,'r-',linewidth=2,label='Linear')
        plt.xlim([0,max(t1)])
        
        plt.figure(4)
        plt.plot(t1,phdot1,'r-',linewidth=2,label='Linear')
        plt.xlim([0,max(t1)])
        
    # Nonlinear simulation
    if nonlinear:
        # Nonlinear Model
        def nonlin(q,t):
            x = q[0]
            ph = q[1]
            xdot = q[2]
            phdot = q[3]
            
            qdot = np.matmul(Minv([x,ph]),R(q,T(q,t)))
    
            return [xdot,phdot,qdot[0],qdot[1]]
        
        # Simulate
        t2 = np.linspace(0,tf,sample)
        q2 = odeint(nonlin,q0,t2)
        
        # phase time of the simulation
        for i in range(len(t2)):
            if t2[i] >= prestart:
                j=i
                break
        t2 = t2[j:]
        t2 = [i-prestart for i in t2]
        
        # Convert back to (cm,deg)
        x2 = q2[j:,0]*100
        ph2 = q2[j:,1]*180/pi
        xdot2 = q2[j:,2]*100
        phdot2 = q2[j:,3]*180/pi
        
        

        # Plots
        plt.figure(1)
        plt.plot(t2,x2,'b-',linewidth=1,label='Non Linear')
        plt.xlim([0,max(t2)])
        
        plt.figure(2)
        plt.plot(t2,ph2,'b-',linewidth=1,label='Non Linear')
        plt.xlim([0,max(t2)])
        
        plt.figure(3)
        plt.plot(t2,xdot2,'b-',linewidth=1,label='Non Linear')
        plt.xlim([0,max(t2)])
        
        plt.figure(4)
        plt.plot(t2,phdot2,'b-',linewidth=1,label='Non Linear')
        plt.xlim([0,max(t2)])
    
    # Plot formating
    plt.figure(1)
    plt.xlabel('Time, [s]')
    plt.ylabel('x position [cm]')
    if nonlinear and linear:
        plt.legend(loc='best')
    plt.savefig(foldername+'/'+foldername+'x.png')
    
    plt.figure(2)
    plt.xlabel('Time, [s]')
    plt.ylabel('pheta position [deg]')
    if nonlinear and linear:
        plt.legend(loc='best')
    plt.savefig(foldername+'/'+foldername+'pheta.png')
    
    plt.figure(3)
    plt.xlabel('Time, [s]')
    plt.ylabel('x velocity [cm/s]')
    if nonlinear and linear:
        plt.legend(loc='best')
    plt.savefig(foldername+'/'+foldername+'xdot.png')
    
    plt.figure(4)
    plt.xlabel('Time, [s]')
    plt.ylabel('pheta velocity [deg/s]')
    if nonlinear and linear:
        plt.legend(loc='best')
    plt.savefig(foldername+'/'+foldername+'phetadot.png')

    plt.figure(1).clear()
    plt.figure(2).clear()
    plt.figure(3).clear()
    plt.figure(4).clear()
    
    mpl.pyplot.clf()
    
# Sample simulation parameters
tf = 0.4
sample = 500    
q0 = [5,0,0,0]

# Sample torques
def T0(q,t):
    return 0
def T1(q,t): 
    return 0.1
def Ti(q,t):
    if t < 0.001:
        return 1000
    else:
        return 0
K = np.matrix([0.3,0.2,0.05,0.02])
def  Tcl(q,t):
    K0 = K[0,0]
    K1 = K[0,1]
    K2 = K[0,2]
    K3 = K[0,3]
    return (K0*q[0]+K1*q[1]+K2*q[2]+K3*q[3])
    

#K = np.matrix([0.05,0.02,0.3,0.2])

# Eigenvalue analysis of closed loop system (for diagnosis)
A2 = np.matrix(A)+np.tensordot(B.transpose().tolist(),K.tolist()[0],0)[0]

print(np.tensordot(B.transpose().tolist(),K.tolist()[0],0)[0])

lam,vec = np.linalg.eig(A)
lam2,vec2 = np.linalg.eig(A2)

#print(A)
#print(B)
#print(lam)
#print(A2)
print(lam2)


# Sample simulatioins

simulate(q0,tf,T0,sample,'linvnonlin1',True)
simulate([5,0,0,0],0.15,T1,sample,'linvnonlin2',True)

simulate([0,0,0,0],1,T0,sample,'3a')
simulate([5,0,0,0],0.4,T0,sample,'3b')
simulate([-5*pi/180*(rc+rb)*100,5,0,0],0.4,T0,sample,'3c')
simulate([0,0,0,0],0.4,Ti,sample,'3d',prestart=0.01)

simulate([5,0,0,0],20,Tcl,sample,'ClosedLoop')

time.sleep(1000)


