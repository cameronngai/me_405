# -*- coding: utf-8 -*-
'''
@file       MCP9808.py
@package    MCP9808
@brief      Class file for control of MCP9808 temperature sensor
@details    Uses MicroPython pyb.I2C library to implement I2C communication 
            and collect temperature data from the sensor.
@author     Cameron Ngai
@author     Ben Sahlin
@date       Feb 4, 2021
'''

import pyb
import utime

class MCP9808:
    BUFFER_LENGTH = 2
    
    def __init__(self,i2c,ADDRESS):
        '''
        @brief          Creates MCP9808 object with specified parameters
        @param i2c      MicroPython I2C object handling communication to MCP9808 device
        @param ADDRESS  I2C bus address corresponding to MCP9808 device
        '''
        #self.i2c = pyb.I2C(bus,pyb.I2C.MASTER,baudrate = self.BAUDRATE)
        self.i2c = i2c
        self.ADDRESS = ADDRESS
        
    def check(self):
        '''
        @brief  Verifies connection with MCP9808
        @returns True if the device is connected successfully and False if it is not
        '''
        return self.i2c.is_ready(self.ADDRESS)
        
    def celcius(self):
        '''
        @brief      Outputs temperature in Celcius
        @details    Measures the ambient temperature value in degrees Celcius using
                    the I2C connected MCP9808 sensor. Assumes that the MCP9808
                    is set to the default resolution of 0.25 C.
        @returns    Returns the measured temperature value as a float in degrees Celcius
        '''
        temp_register_pointer = 5
        self.temp_bytearray = self.i2c.mem_read(self.BUFFER_LENGTH, self.ADDRESS, temp_register_pointer)
        #define most significant byte (MSB) and least significant byte (LSB)
        MSB = self.temp_bytearray[0]     #includes bits 8-15
        LSB = self.temp_bytearray[1]     #includes bits 0-7        
        #check bit 12 for sign of temp (0 = + and 1 = -)
        sign_bit = str(bin(MSB))[4]
        if sign_bit == '0':
            #positive temp value in degrees C
            sign = 1
        else:
            #negative temp value in degrees C
            sign = -1
        #print('sign bit is: '+sign_bit)        
        #mask bits 12 and 13-15 as they are unused for our desired purpose
        MSB_mask = 0b00001111
        MSB = MSB & MSB_mask
        #print('msb after mask is: '+str(MSB))
        #shift upper byte right by 4 bits
        MSB = MSB*2**4
        #print('shifted msb is: '+bin(MSB))
        #shift lower byte left by 2 bits for the two empty bits, then shift left by 4 bits
        #print('lsb is: '+bin(LSB) + 'bitshift by 2: '+bin(LSB<<2))
        LSB = (LSB<<2)*2**-4
        #print('shifted lsb is: '+str(LSB))
        #add shifted upper and lower bytes
        temp = MSB+LSB        
        #temp = self.i2c.recv(self.BUFFER_LENGTH,self.ADDRESS)
        return temp*sign
    
    def fahrenheit(self):
        '''
        @brief      Outputs temperature in Fahrenheit
        @details    Uses Celcius method to measure ambient temperature in degrees 
                    Celcius and then converts the measured value to degrees Fahrenheit.
        @returns    Returns the measured temperature value as a float in degrees Fahrenheit
        '''
        temp = self.celcius()
        temp = temp*9/5+32
        return temp
    
if __name__ == '__main__':
    #test code
    #reads MCP9808 once per second and prints result
    
    #Note: we may need to set up pull up resistors as per handout
    pin_b8 = pyb.Pin(pyb.Pin.cpu.B8, pyb.Pin.PULL_UP)
    pin_b9 = pyb.Pin(pyb.Pin.cpu.B9, pyb.Pin.PULL_UP)
    
    #create i2c obj and specify MCP9808 bus address outside of class initializer
    bus_i2c = 1
    #use standard i2c baudrate as specified in MCP9808 datasheet
    baudrate_i2c = 400000
    i2c = pyb.I2C(bus_i2c, pyb.I2C.MASTER, baudrate = baudrate_i2c)
    address = 0x18
    #create MCP9808 driver object
    mcp = MCP9808(i2c, address)
    
    if mcp.check() == True:
        wait_time = 1000        #wait time in milliseconds
        #print temp reading 1x per second
        try:
            while True:
                temp_c = mcp.celcius()
                temp_f = mcp.fahrenheit()
                print('T = '+str(temp_c)+' C, or '+str(temp_f)+' F')
                utime.sleep_ms(wait_time)
        except(KeyboardInterrupt):
            print('User exited')
    else:
        print('MCP9808 not connected')
