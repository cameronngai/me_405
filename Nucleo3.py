# -*- coding: utf-8 -*-
"""
@file   Nucleo3.py
@package Nucleo3
@brief  Nucleo end of a step response measurer
@details    This file contains the Nucleo end of a step response measurer.
            See \ref UI3 For details
            
            Link to code: https://bitbucket.org/cameronngai/me_405/src/master/Nucleo3.py
            
            The task diagram for this is as below:
                
            @image html lab3task.png
            
@author: Cameron Ngai
@date: January 30, 2021

"""
import pyb
import array


class Nucleo3:
    '''
    @brief      The nucleo end for a step response measurer
    @details    This class facilitates sends data on the step response to the
                user interface
    @image html N3sm.png
    '''
    ## Initialization state
    INIT = 1
    ## Wait for activation state
    IDLE = 2
    ## Wait for button press state
    WAIT = 3
    ## Sending state
    SEND = 4
    
    HZ = 40*10**2
    
    COUNT = 4000
    
    def __init__(self,pin,button,timer):
        
        '''
        @brief  Creates a reflex tester object
        @param  led  The LED that the user reacts to
        @param  button  The button that the user must press
        @param  timer  The timer that times the user reaction time
        @param  refresh_rate  An object that reprensents the time interval between
                checking for updatting states. Note that this refresh rate will not
                impact the timer accuracy.
        '''
        
        # Setup variables
        
        ## The uart to send data through
        self.uart = pyb.UART(2)

        ## The button that the user must press
        self.button = button
        
        ## The timer that times the user reaction time
        self.timer = timer
        self.timer.init(prescaler = 0,period = self.HZ)
        
        self.state = self.INIT
        
        # Settup the callback
        button.irq(self.button_press,pyb.Pin.IRQ_FALLING)
        
        ## Readable pin
        self.pin = pyb.ADC(pin)
        
    def run(self):
        '''
        @brief  Runs the reaction test
        '''
        command = self.get_command()
        # Initialization state
        if self.state == self.INIT:
            # Transition to idle
            self.transition_to(self.IDLE)
            
            # Create buffer arrays for the data
            ## Voltage profile
            self.Vout = array.array ('H', (0 for i in range(self.COUNT)))
            ## Time profile
            self.Tout = array.array ('H', (i for i in range(self.COUNT)))

        elif self.state == self.IDLE:
            # Wait for button press when g is pressed
            if command == ord('g'):
                self.transition_to(self.WAIT)
            
        elif self.state == self.SEND:
            
            # Prepare variables for cutting
            
            self.out = ''
            
            buffer = 10
            filterer = 5
            margin = 100
            maximum = 4096
            
            great = 0
            there = 0
            
            index1 = None
            index2 = None
            
            for i in range(self.COUNT):
                if self.Vout[i] > buffer:
                    # Find begining of plot
                    great += 1
                    if great > filterer:
                        if index1 == None:
                            if i-margin >= 0:
                                index1 = i-margin
                            else:
                                index1 = 0
                else:
                    great = 0
                if self.Vout[i] >  maximum-buffer:
                    # Find end of plot
                    there += 1
                    if there > filterer:
                        if index2 == None:
                            if i+margin < self.COUNT:
                                index2 = i+int(margin/6)
                            else:
                                index2 = self.COUNT-1
                else:
                    great = 0
                
            
            if (index1 == None) | (index2 == None):
                # Reject if fail
                self.out = "e"
            else:
                # Cut out plotted bound
                self.Vout = [self.Vout[i] for i in range(index1,index2)]
                self.Tout = [i for i in range(len(self.Vout))]
                
                # Convert arrays to sendable data
                for i in range(len(self.Vout)):
                    self.out += str(self.Tout[i])+','+str(self.Vout[i])+';'
            
            # Send data
            print(self.out)
            
            # Return to idle
            self.transition_to(self.IDLE)
            
                        
    def transition_to(self,state):
        '''
        @brief  Transitions states
        '''
        # Transition state
        self.state = state
        
        # Transision to testing state
        if state == self.WAIT:
            # Reset buffer
            self.Vout = array.array ('H', (0 for i in range(self.COUNT)))
            self.Tout = array.array ('H', (i for i in range(self.COUNT)))
        
            
            
    def button_press(self,idk):
        '''
        @brief  Callback for button press
        '''
        # Read profile
        self.pin.read_timed(self.Vout,self.timer)  
        # Only move on if in the wait state
        if self.state == self.WAIT:
            self.transition_to(self.SEND)
            #self.timer.counter(0)
                
            
    def get_command(self):
        '''
        @brief  gets command through serial    
        '''
        if self.uart.any() != 0:
            return self.uart.readchar()
        else:
            return None
    


if __name__ == '__main__':
    '''
    @brief      Runs the Nucleo end for the step response measurer
    '''
    
    ## Pin To measure
    pin0 = pyb.Pin(pyb.Pin.cpu.A0, mode = pyb.Pin.OUT_PP)
    ## Button for user to press
    button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
    ## Timer
    t = pyb.Timer(2)
    
    ## Nucleo end object
    r = Nucleo3(pin0,button,t)
    
    # Run test

    while True:
        r.run()