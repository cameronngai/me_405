# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 13:33:38 2021

@author: camer
"""

import pyb
from MotorDriver import MotorDriver
import Encoder as en
import utime

pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)   
pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2,mode = pyb.Pin.IN) # B2
pin_IN1     = pyb.Pin(pyb.Pin.cpu.B5)      
pin_IN2     = pyb.Pin(pyb.Pin.cpu.B4)
pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)      
pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
    # Create the timer object used for PWM generation
tim = pyb.Timer(3,freq = 20000);
   # Create motor objects passing in the pins and timer
moe    = MotorDriver(pin_nSLEEP, pin_nFAULT,pin_IN3, pin_IN4, tim)
   # Enable the motor driver
moe.enable()
  #moe2.enable()
    # Set the duty cycle to 80 percent
moe.set_duty(0)


timer = 4
timer2 = 8
    
    ## First encoder input pin
pinA = pyb.Pin.cpu.B6  # Digital 10
    ## Second encoder input pin
pinB = pyb.Pin.cpu.B7
    
    ## Second encoder input pin
pinA2 = pyb.Pin.cpu.C6  # Digital 10
   ## Second encoder input pin
pinB2 = pyb.Pin.cpu.C7
    
    ## update interval
interval = 0.01
    
    ## Instance of incoder
 
enc = en.Encoder(en.Raw_Encoder(timer2,pinA2,pinB2),interval)

curr_time = pyb.micros()/10**6
next_time = curr_time+0.5
i = 0
state = 0

enc.zero()
while True:
    enc.update()
    
    
    curr_time = pyb.micros()/10**6
    if state == 0:
        if curr_time >= next_time:
            next_time = curr_time+ 0.1
            moe.set_duty(i)
            state = 1
            
    elif state == 1:
        if (curr_time >= next_time) | (enc.get_position() >= 3.14/8):
            speed = enc.get_vel()
            moe.set_duty(-30)
            next_time = curr_time+ 1
            print('PWM: '+str(i))
            print('Speed: '+str(speed)+'\n')
            state = 2
            
    elif state == 2:
        if (curr_time >= next_time) | (enc.get_position() <= 0):
            next_time = curr_time+0.5
            i+=5
            moe.set_duty(0)
            state = 0
            

    if i > 100:
        break
