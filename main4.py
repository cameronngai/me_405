# -*- coding: utf-8 -*-
'''
@file       main4.py
@package    main4
@brief      Main file for Lab 04
@details    This is the main file for Lab 04 and is designed to be run on the 
            Nucleo STM32 board with MicroPython. It collects temperature readings  
            from the MCP9808 sensor at a specified interval and saves the 
            reading to a CSV file. It also collects and saves internal STM32 chip 
            temperatures at each interval. Data collection and saving continues 
            until the user exits using ctrl-C. The MicroPython I2C library along with 
            the MCP9808 class file are used to communicate with the MCP9808 sensor.
            
            We ran this file twice to attain two samples of both the MCP and STM
            temperatures.
            
            @image html temp1.png
            
            This sample ended at 6:05 PM 2/15/21
            
            @image html temp2.png
            
            This sample ended at 6:38 PM 2/15/21
            
@author     Cameron Ngai
@author     Ben Sahlin
@date       Feb 4, 2021
'''

import pyb
import utime
from MCP9808 import MCP9808

if __name__ == '__main__':  
    try:
        wait_time = 10                     # Time between data collection in seconds
        wait_time *= 1000                  # Wait time in milliseconds
        stm = pyb.ADCAll(12, 0x70000)      # ADCALL object initiallization
        
        #set pull up resistors
        pin_b8 = pyb.Pin(pyb.Pin.cpu.B8, pyb.Pin.PULL_UP)
        pin_b9 = pyb.Pin(pyb.Pin.cpu.B9, pyb.Pin.PULL_UP)
        
        #create i2c obj and specify MCP9808 bus address outside of class initializer
        ## Bus type
        bus_i2c = 1
        #use standard i2c baudrate as specified in MCP9808 datasheet
        ## Baudrate of the I2C
        baudrate_i2c = 400000
        i2c = pyb.I2C(bus_i2c, pyb.I2C.MASTER, baudrate = baudrate_i2c)
        ## Address for the MCP9808
        address = 0x18
        #create MCP9808 driver object
        mcp = MCP9808(i2c, address)
        
        # Initialize timing variables
        start_time = utime.ticks_ms()
        next_time = start_time
        
        with open('temperature.csv','w') as file: # File to put data into
        
            file.write('Time [s],STM Temp [C],MCP Temp [C]\n') # Create file headings
    
            while True:
            
                # Time management
                curr_time = utime.ticks_ms()
                if curr_time >= next_time:
                    next_time += wait_time
                    
                    # Data extraction
                    time = curr_time-start_time
                    stm.read_vref()
                    stmTemp = stm.read_core_temp()
                    mcpTemp = mcp.celcius()
                    
                    # File write
                    file.write(str(time/1000)+','+str(stmTemp)+','+str(mcpTemp)+'\n')
                    # Also print temperature readings to console
                    print(str(time/1000)+','+str(stmTemp)+','+str(mcpTemp))

    finally:
        print('done')           # Do when time ends