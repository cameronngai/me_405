# -*- coding: utf-8 -*-
"""
@file simulation.py
@page simulation

@section sec_s0 Introduction

Using the findings in \ref Equations, along with their linearization
the following simulations were run.

@section sec_s1 Linear vs. Nonlinear

Though not required for the assingment, a comparison of the linear and nonlinear
models privides an excelent look into the potential limitations of the linear
model used to design the controller feedback.

The first simulation had an inital contition of (x,p,tdod,tdot)=(5cm,0,0,0) with 
no torque. The resulting plots were as follows

@image html linvnonlin1/linvnonlin1x.png "No Torque Linear vs Nonlinear Simulation, x"
@image html linvnonlin1/linvnonlin1pheta.png "No Torque Linear vs Nonlinear Simulation, theta"
@image html linvnonlin1/linvnonlin1xdot.png "No Torque Linear vs Nonlinear Simulation, xdot"
@image html linvnonlin1/linvnonlin1phetadot.png "No Torque Linear vs Nonlinear Simulation, thetadot"

Here the two simulations are fairly similar. However, when a torque of 0.1 Nm is
applied, the resulting simulations are far from perfect.

@image html linvnonlin2/linvnonlin2x.png "Linear vs Nonlinear Simulation, x"
@image html linvnonlin2/linvnonlin2pheta.png "Linear vs Nonlinear Simulation, theta"
@image html linvnonlin2/linvnonlin2xdot.png "Linear vs Nonlinear Simulation, xdot"
@image html linvnonlin2/linvnonlin2phetadot.png "Linear vs Nonlinear Simulation, thetadot"

The angle does diverge a nontrivial amount in this simulation. Therefore, the
linearization seems to become less precise when the torque is applied. This makes
sense, as the torque should be coupled with the angle, but the linearization
ignores this coupling.


@section sec_s2 Simulation 3A

In the first required simulation, the initial condition is (x,t,xdot,tdot)=(0,0,0,0)
with no torque. 

@image html 3a/3ax.png "3a, x"
@image html 3a/3apheta.png "3a, theta"
@image html 3a/3axdot.png "3a, xdot"
@image html 3a/3aphetadot.png "3a, thetadot"

Since the initial contition is a critical point of the potiential energy of the system, 
all of the state variables remain idle.

@section sec_s3 Simulation 3B

In the second required simulation, the initial condition is (x,t,xdot,tdot)=(5cm,0,0,0)
with no torque. 

@image html 3b/3bx.png "3b, x"
@image html 3b/3bpheta.png "3b, theta"
@image html 3b/3bxdot.png "3b, xdot"
@image html 3b/3bphetadot.png "3b, thetadot"

The divergence of the state variables is not surprising, as the system is unstable.
However, the fact that the x velocity starts negative before diverging in the positive
may require additional explaination, as it may not be present on every simulation.
The x parameter is non inertail, as its origin begins at the center of the table
reguardles of how the table is moving. This initially negative velocity is the
result of the resulting inertial force from D'alembert's principle. This force is
the direct result of the acceleration of the angle of the platform.


@section sec_s4 Simulation 3C

In the third required simulation, the initial condition is (x,t,xdot,tdot)=(x',5deg,0,0)
with no torque. Here, x' is the required position for the ball to be located directly
over the axis of rotation of the platform.

@image html 3c/3cx.png "3c, x"
@image html 3c/3cpheta.png "3c, theta"
@image html 3c/3cxdot.png "3c, xdot"
@image html 3c/3cphetadot.png "3c, thetadot"

This simulation is simpler than the previous, as the only notable phenomina is
the divergence, which is charactoristic of the instabillity involved.

@section sec_s5 Simulation 3D

In the third required simulation, the initial condition is (x,p,xdot,tdot)=(0,0,0,0)
with an impulse torque of 1 Nm*m/s.
Notice that this is equivelent to an initial condition of about 
(x,t,xdot,tdot)=(0,0,3000 cm/s,-40000 deg/s).

@image html 3d/3dx.png "3d, x"
@image html 3d/3dpheta.png "3d, theta"
@image html 3d/3dxdot.png "3d, xdot"
@image html 3d/3dphetadot.png "3d, thetadot"

@section sec_s6 Closed Loop

For the final required simulation , a closed loop controller with gains
(Kx,Kp,Kxdot,Ktdot) = (0.3,0.2,0.05,0.02).

@image html ClosedLoop/ClosedLoopx.png "Closed Loop, x"
@image html ClosedLoop/ClosedLooppheta.png "Closed Loop, theta"
@image html ClosedLoop/ClosedLoopxdot.png "Closed Loop, xdot"
@image html ClosedLoop/ClosedLoopphetadot.png "Closed Loop, thetadot"

Note that this sucessfully attains stabillity.
"""