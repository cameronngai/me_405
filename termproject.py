"""
@file   termproject.py
@package    termproject
@brief      A program to be used to balance a ball on a touch platform
@details    This file will interface with the nucleo to balance a ball on a 
        platform with a touch sensor.

        The origionally desired performance characteristics involved poles of
        -10, -5, -1+0.2i, and -1-0.2 i. In accordance with the equations of motion
        previously derived, the resulting gains are:
            
            Kx = 228
            Kt = 139
            Kdx = 30.7
            Kdt = 13.9
            
        According to the simulation, this should result in the following response:

\htmlonly <style>div.image img[src="CLx1.png"]{width:500px;}</style> \endhtmlonly 
@image html CLx1.png

        When this controller was implemented, performance was suboptimal. One major issue
        was the fact that there was a lower end saturation to the motion of the motor. This
        can be seen in relationship between PWM and motor speed:
\htmlonly <style>div.image img[src="PWM.png"]{width:500px;}</style> \endhtmlonly   
@image html PWM.png 

        When this saturation was eliminated, the controller still did not work. The balancing
        table only worked after some tweaking of the gains:
            
            Kx = 324
            Kt = 126
            Kdx = 92.0
            Kdt = 6.25
            
        According to the simulation, this should result in the following response.
\htmlonly <style>div.image img[src="CLx2.png"]{width:500px;}</style> \endhtmlonly
@image html CLx2.png

        The actual response was fairly distinct, but not remotely far from this 
        prediction. However, it is noteworthy that it took multiple attempts for the system to stabalize.
        
\htmlonly <style>div.image img[src="response.png"]{width:500px;}</style> \endhtmlonly
@image html response.png
        

        The task diagram for the finite state machine used to run the controller is shown below:
\htmlonly <style>div.image img[src="FSMdiagram.png"]{width:500px;}</style> \endhtmlonly
@image html FSMdiagram.png            

        To run the program and balance the ball, the user should balance the plaform 
        as level as possible and then press the blue user button. Once the button 
        is pressed, the platform waits for the ball to be placed. Until the ball 
        is placed the platform balances itself to the  original level. Once the ball
        is placed, the touch platform tracks the ball and that data is used to move 
        the motors to balance the ball. If a fault is encountered, an interrupt 
        is triggered which shuts off the motors. After the user clears the fault all 
        they must do to run the program again is press the blue user button.
        
        The file interaction is demonstrated in the following diagram and links to each of the files used are listed:
\htmlonly <style>div.image img[src="interactdiagram.jpeg"]{width:500px;}</style> \endhtmlonly
@image html interactdiagram.jpeg

Motor: https://bitbucket.org/cameronngai/me_405/src/master/MotorDriver.py
Encoder: https://bitbucket.org/cameronngai/me_405/src/master/Encoder.py
Touch: https://bitbucket.org/cameronngai/me_405/src/master/touch.py

Here is a video explaining our project:
    https://www.youtube.com/watch?v=G2P8ywpq3Jo
@date       March 17, 2021
@author     Cameron Ngai, Brennen Irey, Josephine Isaacson
"""

import pyb
from touch import TCH
from MotorDriver import MotorDriver
import Encoder as en
import array
import utime


class Balance:
    '''
    @brief This class contains the methods and a finite state machine used to balance a rubber ball on a touch platform
    '''
    
    ## The initial state of the FSM
    S0_INIT = 0
    
    ## The state where the FSM waits for the user to press a button
    S1_WAIT = 1
    
    ## The state in which the controller is on 
    S2_ON = 2
    
    ## The state in which the controller is off
    S3_OFF = 3
    
    # Gains
    ## The proportional gain multiplier Kx
    Kx = 228*1.5
    ## Proportional gain Kt
    Kt = 266.7*6/11.5/1.1
    ## Proportional gain for dx
    Kdx = 30.67*3
    ## The proportional gain for dt
    Kdt = 26.66*6/11.5/2/1.1
    
    # Gains for table alone
    Ktt = 493.333*6/11.5/2
    Kdtt = 49.333*6/11.5/4
    
    # Filter parameters
    ##filter buf
    buf_len = 15
    ##boolean buf
    bool_buf = 25
    
    RC = 20
    
    def __init__(self,tc,mox,moy,encx,ency,button):
        '''
        @brief Creates the Balance task, takes inputs, and sets up objects to be used
        @param  tc  The touch screen
        @param  mox  The motor controlling the x position
        @param  moy  The motor controlling the y position
        @param  encx  The encoder for the x position motor
        @param  moy  The encoder for the x position motor
        @param  moy  The button that allows to user to change the state
        @param ency The encoder for the y position motor
        @param button The button to be used for the interrupt
        '''

        ## An object copy of the touch panel class
        self.TCH = tc
        
        ## motor X 
        self.mox = mox
        
        ## Motor Y
        self.moy = moy
        
        ## Encoder x
        self.encx = encx
        
        ## Encoder Y
        self.ency = ency
        
        # initialize the x encoder by setting it to zero
        self.encx.zero()
        
        # initialize the y encoder by zeroing the position
        self.ency.zero()
        
        ## The state to be run on the first iteration of the task
        self.state = self.S0_INIT
        
        ## The current location of the ball in the x direction
        self.bx = self.TCH.X()
        
        ## the current position of the ball in the y direction
        self.by = self.TCH.Y()
        
        ## Zeroing postition and time variables        
        self.bvx = 0
        ## Zero Y value
        self.bvy = 0     
       
        ## Zero the x variable
        self.x = 0
        ## Zero the Y variable
        self.y = 0       
        ## Xposition
        self.bxp = None
        ## Y position
        self.byp = None
        ## derivative
        self.dt = None
        ## The previous time
        self.prev_time = None
        ## x buffer
        self.bx_buf = [0]*self.buf_len
        ## y buffer
        self.by_buf = [0]*self.buf_len
        ##derivative buffer
        self.dt_buf = [0]*self.buf_len
        #self.bvx_buf = [0]*self.buf_len
        #self.bvy_buf = [0]*self.buf_len
        
        ## The Z buffer
        self.z_buf = [False]*self.bool_buf
        ## Z variable initially zero
        self.z = False
        
        ## Setting motor power low for motor x
        self.PWMx = 0
        ## Y motor being set low
        self.PWMy = 0
        #self.curr_time = pyb.micros()/10**6

        ## The interrupt
        button.irq(self.buttonpress, pyb.Pin.IRQ_FALLING)
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''

        #self.curr_time = pyb.micros()/10**6
        
        # if the FSM is in state 0, transition to S1 and track the ball
        if self.state == self.S0_INIT:

            self.state = self.S1_WAIT
            self.trackball()
            
        # State when ball is on the table                    
        elif self.state == self.S2_ON:
            self.onbal()
            if ((not self.mox.safe)|(not self.moy.safe)):
                self.state = self.S1_WAIT
                print('shut down')
            elif not self.z:
                self.state = self.S3_OFF
                print('off')
            
        # State when ball is off of the table
        elif self.state == self.S3_OFF:
            
            self.offbal()
            if ((not self.mox.safe)|(not self.moy.safe)):
                self.state = self.S1_WAIT
                print('shut down')
            elif self.z:
                self.state = self.S2_ON
                print('on')
                
        self.trackball()
    
    def buttonpress(self, idk):
        '''
        @brief This method is the method to be used in the interrupt. It handles
                state transitions caused by pressing the button
        @param idk input
        '''
        if self.state == self.S1_WAIT:
            
            ## Prepares hardware by zeroing encoder position and motor power.
            self.encx.zero()
            self.ency.zero()
            self.mox.set_duty(0)
            self.moy.set_duty(0)
            self.mox.clear()
            self.moy.clear()
            
            self.dt = None
            self.prev_time = None
            
            ## Initializes data storage arrays
            self.x_arr = array.array('f')
            ## Y data storage array
            self.y_arr = array.array('f')
            ## Time storage array
            self.times = array.array('f')
        
            
            #check if the ball is in contact with the board
            if self.z:
                self.state = self.S2_ON
                print('on')
            
            else:
                self.state = self.S3_OFF
                print('off')
        else:
            
            ## Turns off motors and resets FSM
            self.mox.disable()
            self.moy.disable()
            self.state = self.S1_WAIT
            
            ## Writes saved data to CSV upon pushing the button a second time
            with open('TermPrpjectData.csv', 'w') as file:
                file.write('x,y,time\r')
                for n in range(len(self.times)):
                    file.write('{:},{:},{:}\r'.format(self.x_arr[n], self.y_arr[n], self.times[n]))
                file.close()
                    
    def onbal(self):
        '''
        @brief This method is the on balance method which makes the motors balance the ball on the platform
        '''
        # update each encoder
        self.encx.update()
        self.ency.update()
        
        #calc the PWM value for each motor
        # the plus 10 accounts for the weight of the motor arms
        PWMx = -(self.Kt*self.encx.get_position()+self.Kdt*self.encx.get_vel())-(self.Kx*self.bx+self.Kdx*self.bvx)+10
        PWMy = -(self.Kt*self.ency.get_position()+self.Kdt*self.ency.get_vel())+(self.Kx*self.by+self.Kdx*self.bvy)+10
        
        
        #Eliminate lower saturation
        offset = 20
        if PWMx > 0:
            PWMx += offset
        elif PWMx < 0:
            PWMx -= offset
            
        if PWMy > 0:
            PWMy += offset
        elif PWMy < 0:
            PWMy -= offset
            
        self.PWMx = PWMx
        self.PWMy = PWMy
            
        # Run
        self.mox.set_duty(PWMx)
        self.moy.set_duty(PWMy)
        
        # Track data 
        self.x_arr.append(self.bx)
        self.y_arr.append(self.by)
        self.times.append(utime.ticks_us())
        
        
    def offbal(self):
        '''
        @brief This method is the off balance method which makes the motors balance the platform alone
        '''
        # Update encoder
        self.encx.update()
        self.ency.update()
        
        
        #calc the PWM value for each motor
        # the plus 10 accounts for the weight of the motor arms
        PWMx = -(self.Ktt*self.encx.get_position()+self.Kdtt*self.encx.get_vel())+7
        PWMy = -(self.Ktt*self.ency.get_position()+self.Kdtt*self.ency.get_vel())+7
        
        
        #Lower saturation at 20 PWM
        if PWMx > 0:
            PWMx += 20
        elif PWMx < 0:
            PWMx -= 20
            
        if PWMy > 0:
            PWMy += 20
        elif PWMy < 0:
            PWMy -= 20

        self.mox.set_duty(PWMx)
        self.moy.set_duty(PWMy)
        
        
    def trackball(self):
        '''
        @brief This method tracks the ball on the platform
        @details This method checks if the ball is on the platform and then updates where the ball is on the platform
        '''
        
        # Grabs the current positional data from touchscreen
        bx = self.TCH.X()
        by = self.TCH.Y()
        z = self.TCH.Z()
        
        self.z_buf += [z]
        
        self.z_buf = self.z_buf[1:]
        
        # Determine whether the ball is on by takng the majority
        if sum(self.z_buf)/self.bool_buf > 0.5:
            self.z = True
        else:
            self.z = False
            
        # Update if both agree that the ball is on the table
        if (z == self.z)&(z == True):
            
            self.bx_buf += [bx]
            self.by_buf += [by]
            
            self.bx_buf = self.bx_buf[1:]
            self.by_buf = self.by_buf[1:]
            
            # Find difference in time
            dt = None
            if self.prev_time != None:
                dt = pyb.micros()/10**6-self.prev_time
                
                self.dt_buf += [dt]
                self.dt_buf = self.dt_buf[1:]
                
            self.prev_time = pyb.micros()/10**6
        
            # Find velocity.
            if dt != None:
                self.bvx = (bx-self.bx_buf[-2])/dt
                self.bvy = (by-self.by_buf[-2])/dt    
            else:
                self.bvx = 0
                self.bvx = 0
        
        # Low pass filter
        a = self.dt_buf[0]/(self.RC+self.dt_buf[0])
        x = [0]*self.buf_len
        x[0] = self.bx_buf[0]
        y = [0]*self.buf_len
        y[0] = self.by_buf[0]
        for i in range(1,self.buf_len):
            a = self.dt_buf[i]/(self.RC+self.dt_buf[i])
            x[i] = a * self.bx_buf[i] + (1-a) * x[i-1]
            y[i] = a * self.by_buf[i] + (1-a) * y[i-1]
            
        # Set position values
        self.bx = x[-1]
        self.by = y[-1]
        self.x = x
        self.y = y
        
        # Reject unrealistic velocities
        if abs(self.bvx) >= 1:
            self.bvx = 0
        if abs(self.bvy) >= 1:
            self.bvy = 0
        
        
        
if __name__ =='__main__':
        
    
    # Motor Setup
    
    # Create the pin objects used for interfacing with the motor drivers      
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)   
    pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2,mode = pyb.Pin.IN) # B2
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B5)      
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)      
    pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3,freq = 20000);
    # Create motor objects passing in the pins and timer
    moe2     = MotorDriver(pin_nSLEEP, pin_nFAULT,pin_IN1, pin_IN2, tim)
    moe    = MotorDriver(pin_nSLEEP, pin_nFAULT,pin_IN3, pin_IN4, tim)
    
    # Encoder Setup
    ## Timer type for encoder
    timer = 4
    timer2 = 8
    
    ## First encoder input pin
    pinA = pyb.Pin.cpu.B6  # Digital 10
    ## Second encoder input pin
    pinB = pyb.Pin.cpu.B7
    
    ## Second encoder input pin
    pinA2 = pyb.Pin.cpu.C6  # Digital 10
    ## Second encoder input pin
    pinB2 = pyb.Pin.cpu.C7
    
    ## update interval
    interval = 0.0001
    
    ## Instance of incoder
    enc2 = en.Encoder(en.Raw_Encoder(timer,pinA,pinB),interval)
    enc = en.Encoder(en.Raw_Encoder(timer2,pinA2,pinB2),interval)
    ## Instance of interface
    
    
    # Touch sensor
    
    ## Positive x pin
    xp = pyb.Pin.cpu.A0
    ## Negative x pin
    xm = pyb.Pin.cpu.A6
    ## Positive y pin
    yp = pyb.Pin.cpu.A7
    ## Negative y pin
    ym = pyb.Pin.cpu.A1
    
    ## Length along x direction
    Lx = 0.17664
    ## Length along y direction
    Ly = 0.09936
    
    ## X origin
    cx = Lx/2
    ## Y origin
    cy = Ly/2
    
    ## TCH object
    tch = TCH(xp,xm,yp,ym,Lx,Ly)
    
    # button
    ## Button
    button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
    
    control = Balance(tch,moe,moe2,enc,enc2,button)
    control.mox.enable()
    control.moy.enable()
    while True:
        control.run()
    
            