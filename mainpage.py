## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage 
#
#  @section sec_intro Introduction
#  The following is a portfolio for Cameron Ngai's work in ME 405
#
#  @section sec_1 Lab1
#  This is the first lab, where a vending machine is simulated
#  
#  Lab1 documentation: \ref lab1 package.\n
#  Link to code: https://bitbucket.org/cameronngai/me_405/src/master/lab1.py
#
#  @section sec_2 Lab2
#  This is the second lab, where a reflex tester is implemented on the Nucleo
#  
#  Lab2 documentation: \ref lab2 package.\n
#  Link to code: https://bitbucket.org/cameronngai/me_405/src/master/lab2.py
#
#  @section sec_3 Lab3
#  This is the third lab, where a step response is measured
#  
#  Lab3 documentation: \ref UI3 package, \ref Nucleo3 package.\n
#                       
#  Link to code: https://bitbucket.org/cameronngai/me_405/src/master/UI3.py \n
#  Link to code: https://bitbucket.org/cameronngai/me_405/src/master/Nucleo3.py
#
#  @section sec_4 Lab4
#  This is the third lab, where I work in a team to measure temperuratures
#
#  Lab4 documentation: \ref main4 package, \ref MCP9808 package
#
#  Link to code: https://bitbucket.org/bsahlin/me405_labs/src/master/Lab4/main4.py \n
#  Link to code: https://bitbucket.org/bsahlin/me405_labs/src/master/Lab4/MCP9808.py
#
#  @section sec_5 Lab5
#  This is the fith lab, where the equations of motions for a mechanical system are derived.
#
#  Link to page: \ref Equations page
#
#  @section sec_6 Lab6
#  This is the sixth lab, where the mechanical simulation of Lab 5 are simulated
#
#  Link to page: \ref simulation
#
#  Link to code:  https://bitbucket.org/cameronngai/me_405/src/master/sim.py
#
#  @section sec_7 Lab7
#  This is the seventh lab, where a touch sensor driver is developed
#
#  Lab7 documentation: \ref touch package
#
#  Link to code:  https://bitbucket.org/cameronngai/me_405/src/master/touch.py
#
#  @section sec_8 Lab8
#  This is the eigth lab, where motor controlling packages were created. This was done in a team.
#
#  MotorDriver documentation: \ref MotorDriver package
#
#  Encoder documentation: \ref Encoder package
#
#
#  Link to Motor code:  https://bitbucket.org/cameronngai/me_405/src/master/MotorDriver.py
#
#  Link to Encoder code:  https://bitbucket.org/cameronngai/me_405/src/master/Encoder.py
#
#  
#  @section sec_9 Lab9
#  This is the final lab, where, as a team, a ball balancing program was created.
#
#  Project documentation: \ref termproject package
#
#  Link to Motor code:  https://bitbucket.org/cameronngai/me_405/src/master/termproject.py
#
#
#  @author Cameron Ngai

#
#  @date January 14, 2021
#