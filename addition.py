# -*- coding: utf-8 -*-
"""
@file addition.py
@package addition
@author     Cameron Ngai
@brief      A function that adds natural numbers
@details    This function uses binary operations to add natural numbers
"""

def add(a,b):
    '''
    @brief      A function that adds natural numbers.
    @details    This function uses binary operations to add natural numbers
    @param  a   The first number to add
    @param  b   The first number to add
    '''
    while a&b:
        a1 = a^b
        b = (a&b) << 1 
        a = a1
    a = a^b
    return a