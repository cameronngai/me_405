# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 14:02:09 2021

@author: camer
"""

import keyboard


# pushed_key = None

# def on_keypress (thing):
#     """ Callback which runs when the user presses a key.
#     """
#     global pushed_key

#     pushed_key = thing.name

# keyboard.on_press (on_keypress)  ########## Set callback

# # Run a simple loop which responds to some keys and ignores others.
# # If someone presses control-C, exit this program cleanly
# while True:
#     try:
#     # If a key has been pressed, check if it's a key we care about
#         if pushed_key:
#             if pushed_key == "0":
#                 print ("Penny")
#             elif pushed_key == '1':
#                 print ("Nickel")
#             elif pushed_key == 'e':
#                 print ("Eject")
#             pushed_key = None

#         # If Control-C is pressed, this is sensed separately from the keyboard
#         # module; it generates an exception, and we break out of the loop
#     except KeyboardInterrupt:
#         break

# print ("Control-C has been pressed, so it's time to exit.")
#     #keyboard.unhook_all ()

keyp = None
def on_keypress(key):
    global keyp
    if key == keyboard.KeyboardEvent('down',key.scan_code):
        keyp = 'down '+key.name
    else:
        keyp = 'up '+key.name



keyboard.hook(on_keypress)
while True:
    if keyp:
        print(keyp)
        keyp = None

    
