# -*- coding: utf-8 -*-
'''
@file       touch.py
@package touch
@brief      Class file for use of TCH touch sensor
@details    Drives the TCH touch sensor. Outputs the x position, the y position,
            and a boolian determining whether the ball is on the table. The
            hardware settup appears as imaged below.
\htmlonly <style>div.image img[src="lab7full.jpg"]{width:500px;}</style> \endhtmlonly
@image html lab7full.jpg 
            The electronics are soldered and attached to the board as below:
\htmlonly <style>div.image img[src="lab7elec.jpg"]{width:500px;}</style> \endhtmlonly
@image html lab7elec.jpg 
\htmlonly <style>div.image img[src="lab7elec2.jpg"]{width:500px;}</style> \endhtmlonly
@image html lab7elec2.jpg 
            When sucessfully implemented, the results should appear as in the 
            following image
\htmlonly <style>div.image img[src="lab7data.jpg"]{width:500px;}</style> \endhtmlonly 
@image html lab7data.jpg 

@author     Cameron Ngai
@date       Feb 4, 2021
'''

import pyb
import utime

class TCH:
    '''
    @brief      Class that drives a TCH touch sensor
    @details    Drives a TCH touch sensor by collecting the x position, y position,
                and boolian describing whether touch is sensed at all.
    '''
    NORMALIZER = 4095
    Lx = 0.17664
    Ly = 0.09936
    
    def __init__(self,xp,xm,yp,ym,Lx,Ly,cx=None,cy=None):
        '''
        @brief          Creates TCH object with specified parameters
        @param xp       Positive x pin
        @param xm       Negative x pin
        @param yp       Positive y pin
        @param ym       Negative y pin
        @param Lx       Length along the x direction
        @param Ly       Length along the y direction
        @param cx       x origin
        @param cy       y origin
        '''
        
        # Setup Parameters
        self.xp = xp
        self.xm = xm
        self.yp = yp
        self.ym = ym
        self.Lx = Lx
        self.Ly = Ly
        
        # Setup list for samples
        ## Number of samples to take for each measurement
        self.sample = 2
        ## Set of samples taken for each measurement
        self.sample_set = []
        for i in range(self.sample):
            self.sample_set += [0]
        
        # setup origin if none given
        if cx == None:
            self.cx = self.Lx/2
        else:
            self.cx = cx
        if cy == None:
            self.cy = self.Ly/2
        else:
            self.cy = cy
        
        
    def X(self):
        '''
        @brief          Outputs the x position of the touch screen
        '''
        
        # setup pin configurations
        pin_xp = pyb.Pin(self.xp)
        pin_xm = pyb.Pin(self.xm) 
        pin_xp.init(mode = pyb.Pin.OUT_PP, value = 1)
        pin_xm.init(mode = pyb.Pin.OUT_PP,value = 0)
        pin_yp = pyb.ADC(self.yp)
        pin_ym = pyb.ADC(self.ym)
        
        # collect sample of data        
        for i in range(self.sample):
            self.sample_set[i] = pin_ym.read()
        
        # average results
        x = sum(self.sample_set)/self.sample
        
        # convert to cm and send
        return x/self.NORMALIZER*self.Lx-self.cx
        
    def Y(self):
        '''
        @brief          Outputs the y position of the touch screen
        '''
        
        # setup pin configurations
        pin_yp = pyb.Pin(self.yp)
        pin_ym = pyb.Pin(self.ym) 
        pin_yp.init(mode = pyb.Pin.OUT_PP, value = 1)
        pin_ym.init(mode = pyb.Pin.OUT_PP,value = 0)
        pin_xp = pyb.ADC(self.xp)
        pin_xm = pyb.ADC(self.xm)
        
        # collect sample of data
        for i in range(self.sample):
            self.sample_set[i] = pin_xm.read()
        
        # average results
        y = sum(self.sample_set)/self.sample
        
        # convert to cm and send
        return  y/self.NORMALIZER*self.Ly-self.cy
    
    def Z(self):
        '''
        @brief          Outputs a boolian that is True when touch is detected
        '''
        
        # setup pin configurations
        pin_yp = pyb.Pin(self.yp)
        pin_xm = pyb.Pin(self.xm) 
        pin_yp.init(mode = pyb.Pin.OUT_PP, value = 1)
        pin_xm.init(mode = pyb.Pin.OUT_PP,value = 0)
        pin_xp = pyb.ADC(self.xp)
        pin_ym = pyb.ADC(self.ym)
        
        # Delay time to get more acurate result
        utime.sleep(0.000005)
        
        # Determine if touch is sensed
        if pin_xp.read() <= 2:
            return False
        else:
            return True
        
    def collect(self):
        '''
        @brief          Collects all values
        '''
        return (self.X(),self.Y(),self.Z())

      

if __name__ == '__main__':  
    # Test code
    
    ## Positive x pin
    xp = pyb.Pin.cpu.A0
    ## Negative x pin
    xm = pyb.Pin.cpu.A6
    ## Positive y pin
    yp = pyb.Pin.cpu.A7
    ## Negative y pin
    ym = pyb.Pin.cpu.A1
    
    ## Length along x direction
    Lx = 0.17664
    ## Length along y direction
    Ly = 0.09936
    
    ## X origin
    cx = Lx/2
    ## Y origin
    cy = Ly/2
    
    ## TCH object
    tch = TCH(xp,xm,yp,ym,Lx,Ly)
    
    try:
        while(True):
            
            # Collect and time collection
            start = utime.ticks_us()
            val = tch.collect()
            time = utime.ticks_diff(utime.ticks_us(),start)
            
            # Print results
            print('Total Time [us]: '+str(time)+'\n')
            print('(X [cm],Y [cm],Z [on/off]) = ')
            print('('+str(round(val[0],4)*100)+',\n'+str(round(val[1],4)*100)+',\n'+str(val[2])+')')
            print('\n\n')
            
            # Wait some time
            utime.sleep(0.001)

    finally:
        print('done')
    
    

 
 
 
 