# -*- coding: utf-8 -*-
"""
@file   lab2.py
@package lab2
@brief  A reflex tester
@details    This file contains a reflex tester that operates on the Nucleo board.
            It will turn on a LED, at which point the user must press a button as
            quickly as possible. Upon termination, the system will reveal information
            about the users reflex time.
            
            Link to code: https://bitbucket.org/cameronngai/me_405_labs/src/master/Lab2/lab2.py

@author: Cameron Ngai
@date: January 21, 2021

"""
import pyb
import random

class reflex_test:
    '''
    @brief  A task that tests the users reflex
    @details    This object will turn on a light for a second, and the user must
                press the button as quickly as possible. Upon exiting the program,
                the reaction times will be measured and averaged for the user to see.
    The finite state machine for this task is as below:
    @image html LAB2SM.jpg
    '''
    ## Initialization state
    INIT = 1
    ## Waiting state
    WAIT = 2
    ## Testing state
    GO = 3
    
    HZ = 80*10**6
    
    def __init__(self,led,button,timer,refresh_rate):
        
        '''
        @brief  Creates a reflex tester object
        @param  led  The LED that the user reacts to
        @param  button  The button that the user must press
        @param  timer  The timer that times the user reaction time
        @param  refresh_rate  An object that reprensents the time interval between
                checking for updatting states. Note that this refresh rate will not
                impact the timer accuracy.
        '''
        
        # Setup variables
        
        ## The LED that the user reacts to
        self.led = led
        ## The button that the user must press
        self.button = button
        ## An object that reprensents the time interval between checking for updatting states.
        self.ref_rate = refresh_rate
        ## The timer that times the user reaction time
        self.timer = timer
        self.timer.init(prescaler = 0,period = 0x7FFFFFFF)
        
        self.state = self.INIT
        
        # Settup the callback
        button.irq(self.button_press,pyb.Pin.IRQ_FALLING)
        
        ## Stores the reaction times
        self.times = []
        
    def run(self):
        '''
        @brief  Runs the reaction test
        '''
        # Initialization state
        if self.state == self.INIT:
            
            # Initialize variables
            self.init_time = pyb.micros()/10**6
            self.curr_time = self.init_time
            self.ref_time = self.curr_time + self.ref_rate
            self.times = []
            
            # Transition to wait
            self.transition_to(self.WAIT)
            
        else:
            # Runs non-initialization states when refresh is called
            self.curr_time = pyb.micros()/10**6
            if self.curr_time >= self.ref_time:
                # Waiting state
                if self.state == self.WAIT:
                    # Transition state when time elapses
                    if self.curr_time >= self.next_time:
                        self.transition_to(self.GO)
                        
                # Testing state
                elif self.state == self.GO:
                    # Transition state if button is pressed or time elapses
                    if (self.curr_time >= self.next_time)|self.pressed:
                        self.transition_to(self.WAIT)
                        
    def transition_to(self,state):
        '''
        @brief  Transitions states
        '''
        # Transition state
        self.state = state
        
        # Transision to wait state
        if state == self.WAIT:
            # Settup a random state transition time between 2 and 3
            self.next_time = self.curr_time + 2 + random.randint(0,1000)/1000
            # Turn off led
            self.led.low()
            
            # Reject unfilled array elements
            trys = len(self.times)
            if trys != 0:
                if self.times[trys-1] == None:
                    del self.times[trys-1]
        
        # Transision to testing state
        elif state == self.GO:
            # Settup the state to transition in one second
            self.next_time = self.curr_time + 1
            
            # The button is not pressed
            self.pressed = False
            
            # Restart timer
            self.timer.counter(0)
            
            # Turn on LED
            self.led.high()
            
            # Prealocate time date for interupt
            self.times += [None]
            
    def button_press(self,idk):
        '''
        @brief  Callback for button press
        '''
        # Only collect data if in the go state
        if self.state == self.GO:
            self.pressed = True
            self.times[len(self.times)-1] = self.timer.counter()
            
    def end(self):
        '''
        @brief  Call when the program is closed
        '''
        if len(self.times) == 0:
            print('\nNo reaction data collected.\n\n')
        else:
            # Convert times to milliseconds
            trys = len(self.times)
            self.times = [i / self.HZ*1000 for i in self.times]
                        
            # Reject unfilled array elements
            if self.times[trys-1] == None:
                del self.times[trys-1]
            
            # Compute statistics
            average = sum(self.times)/trys
            best = min(self.times)
            
            # Print statistics
            print('\nYou tested your reflexes ' +str(trys)+ ' times.')
            print('Your average reaction time was '+str(average)+' milliseconds.')
            print('Your best reaction time was '+str(best)+' milliseconds.\n\n')


if __name__ == '__main__':
    '''
    @brief      Runs the reflex test
    '''
    ## Refresh rate
    refresh_rate = 0.05
    
    ## Pin for the LED
    pin5 = pyb.Pin(pyb.Pin.cpu.A5, mode = pyb.Pin.OUT_PP)
    ## Button for user to press
    button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
    ## Timer
    t = pyb.Timer(2)
    
    ## Reflex test object
    r = reflex_test(pin5,button,t,refresh_rate)
    
    # Run test
    try:
        while True:
            r.run()
    finally:
        r.end()