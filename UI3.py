# -*- coding: utf-8 -*-
"""
@file   UI3.py
@package UI3
@brief  UI end of a step response measurer
@details    This file contains the user interface end of a step response measurer.
            When the user presses g, the nucleo will be ready to recieve a tap
            on the button. After this tap, the voltage step response will be collected
            and published. This file works in conjunction with \ref Nucleo3
            
            An example step response is as below:
            @image html stepResponse.png
            
            Link to code: https://bitbucket.org/cameronngai/me_405/src/master/UI3.py
            
            The task diagram for this is as below:
                
            @image html lab3task.png
            
@author: Cameron Ngai
@date: January 30, 2021

"""

import keyboard
import serial
import matplotlib as mpl

class keys:
    '''
    @brief      A class that keeps track of the keys pressed
    @details    This class will keep track of the keyboard key. The key can be 
                put into a command variabe, which can also be deleted.
    '''
    ## Comand given by the keyboard
    command = None
    key = None
    was_key = None
    held = False
    def __init__(self):
        keyboard.hook(self.on_keyevent)
    def on_keyevent (self,thing):
        '''
        @brief  This method is a callback for the keyboard input events
        @param  thing   The keyboard event
        '''
        if thing == keyboard.KeyboardEvent('down',thing.scan_code):
            if thing.name != self.was_key:
                self.key = thing.name
                self.was_key = self.key
        else:
            self.was_key = None
            
    def get_command(self):
        '''
        @brief  This method puts the existing key value into the command value
        '''
        
        self.command = self.key
        self.key = None
    def kill_command(self):
        '''
        @brief  This method cleared the command variable
        '''
        self.command = None

## This object will keep track of what key is pressed
k = keys()

class UI3:
    '''
    @brief      A user interface for step response measurer
    @details    This class facilitates a user interface with the Nucleo to measure
                the step response when the button is pressed. The state diagram is
                as below:
    @image html UI3sm.png
    '''
    
    # States
    ## Initialization state
    INIT = 1
    ## Idle state
    IDLE = 2
    ## Collection state
    COLLECT = 3

    
    def __init__(self):
        '''
        @brief  Creates an instance of the user interface
        '''
        self.state = self.INIT
    
        self.ser = serial.Serial(port='COM8',baudrate=115273,timeout=1)
        
    def run(self):
        '''
        @brief  Runs the user interface
        '''
        
        # Gets command from the keyboard
        command = k.command

        if self.state == self.INIT:
            # Begin at idle state
            self.state = self.IDLE
            self.print_menu()
            
        elif self.state == self.IDLE:
            # Tell Nucleo to get ready to collect when g is pressed on the keyboard
            if command == 'g':
                print('Collecting Data: Tap button when ready')
                self.send_command('g')
                self.state = self.COLLECT
                
        elif self.state == self.COLLECT:
            # Wait for data to exist in the serial
            if self.ser.in_waiting > 0:
                raw = self.ser.readline().decode('ascii')
                if raw == "e\r\n":
                    # Get rid of faulty data
                    print("Error: Bump not captured, try tapping the button faster")
                    self.send_command('g')
                else:
                    
                    # Extract data from recieved string
                    less_raw = raw.split(';')
                    del less_raw[-1]
                    ## Voltage profile
                    self.Vout = [int(s.split(',')[1]) for s in less_raw]
                    ## Time profile
                    self.Tout = [int(s.split(',')[0]) for s in less_raw]
                    
                    if self.Vout[0] > 3000:
                        # Get rid of trivial data
                        print("Error: Bump not captured, try tapping the button slower")
                        self.send_command('g')
                    else:
                        
                        print("Data successfully captured")
                        
                        # Convert data to normal units
                        self.Tout = [t*5*10**(-2) for t in self.Tout]
                        self.Vout = [v* 3.3/4096 for v in self.Vout]
                        
                        # Plot data
                        mpl.use('AGG')
                        mpl.pyplot.plot(self.Tout,self.Vout)
                        
                        mpl.pyplot.xlabel('Time, ms')
                        mpl.pyplot.ylabel('Voltage, V')
                
                        mpl.pyplot.savefig('stepResponse.png')
                        mpl.pyplot.clf()
                        
                        # Write data into csv file
                        file = open('stepResponse.csv','w')
                        file.write('Time [ms],Voltage [V]\n')
                        for i in range(len(self.Tout)):
                            file.write(str(self.Tout[i])+','+str(self.Vout[i])+'\n')
                        
                        # Return to idle
                        self.state = self.IDLE
                        self.print_menu()
                        
    def print_menu(self):
        print('Press g to begin collecting data')
        
    def send_command(self,command):
        '''
        @brief  Sends command through serial
        @param  command  The command sent through the serial    
        '''
        self.ser.write(str(command).encode('ascii'))
    

if __name__ == '__main__':
    '''
    @brief      Runs the user interface
    '''
    ui = UI3()
    while True:
        k.get_command()
        ui.run()
        k.kill_command()
    
    
    