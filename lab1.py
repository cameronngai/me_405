# -*- coding: utf-8 -*-
"""
@file   lab1.py
@package    lab1
@author     Cameron Ngai
@brief      A set of functions and classes constructing a vending machine
@details    This module includes a class that simulates a vending machine that can
            accept change, eject change, and determine if enough change has been given
            for a certain beverage. The inputs for the vending machine are accepted
            through a keyboard in a cooperative way.
            A state machine for the system is as below:
            @image html lab1sm.png
            The following is a link to the code: https://bitbucket.org/cameronngai/me_405/src/master/lab1.py
"""

import numpy as np
import math
import keyboard
import time


class keys:
    '''
    @brief      A class that keeps track of the keys pressed
    @details    This class will keep track of the keyboard key. The key can be 
                put into a command variabe, which can also be deleted.
    '''
    ## Comand given by the keyboard
    command = None
    key = None
    was_key = None
    held = False
    def __init__(self):
        keyboard.hook(self.on_keyevent)
    def on_keyevent (self,thing):
        '''
        @brief  This method is a callback for the keyboard input events
        @param  thing   The keyboard event
        '''
        if thing == keyboard.KeyboardEvent('down',thing.scan_code):
            if thing.name != self.was_key:
                self.key = thing.name
                self.was_key = self.key
        else:
            self.was_key = None
            
    def get_command(self):
        '''
        @brief  This method puts the existing key value into the command value
        '''
        
        self.command = self.key
        self.key = None
    def kill_command(self):
        '''
        @brief  This method cleared the command variable
        '''
        self.command = None

## This object will keep track of what key is pressed
k = keys()
#keyboard.hook(k.on_keyevent)


def getChange(value):
    '''
    @brief      A function that finds the change for a value of money
    @details    This function inputs a value of money, then determines the change
                required to equal the value that involves the least amount of currency objects
    @param  value   The value of money to find the change for
    '''
    
    money = (0.01,0.05,0.10,0.25,1,5,10,20)
    l = len(money)
    
    # Check price resonability
    if isinstance(value,float) | isinstance(value,int):
        if value == abs(value):
            if value*100 != int(value*100):
                print('Error: Price cannot be more precise than one cent')
                return None
        else:
            print('Error: Value must be a positive value')
            return None
    else:
        print('Error: Value must be a numeric value')
        return None
    
    # Get necissary change
    if value == 0:
        return (0,0,0,0,0,0,0,0)

    nchange = round((value),2)
    
    # Get minimum change
    change = np.zeros(l)
    for i in range(l):
        j = l-1-i
        change[j] = math.floor(nchange/money[j])
        nchange -= change[j]*money[j]
    change = tuple(change)
    return change


class vendatron:
    '''
    @brief      A simulation of a vending machine.
    @details    This class simulates a vending machine. The vending machine has
                multiple beverage options, each with a different cost. A user can
                insert change, accumulating a balance. The user can use the balance
                to purchase the beverage or eject all of the balance back into coins.
                The state machine is as below:
    @image html lab1sm.png
    '''
    INIT = 1
    IDLE = 2
    COUNT = 3
    
    ## Dictionary carrying the prices fro each type of beverage
    DRINKS = {"Cuke":1.00,"Popsi":1.20,"Spryte":0.85,"Dr. Pupper":1.10}
    
    ## Dictionary carrying the shortcuts for each type of beverage
    DRINKSHORT = {"Cuke":"c","Popsi":"p","Spryte":"s","Dr. Pupper":"d"}
    
    ## Labels for each type of currency
    MONEY_LABEL = ('pennies','nickels','dimes','quarters','ones','fives','tens','twenties')
    
    ## Values for each type of currency
    MONEY = (0.01,0.05,0.10,0.25,1,5,10,20)
    
    def __init__(self):
        '''
        @brief  Creates an instance of the vendatron
        '''
        self.state = self.INIT
        self.balance = 0
        self.SHORTDRINK = {value : key for (key, value) in self.DRINKSHORT.items()}
        
        
    def run(self):
        '''
        @brief  Runs an instance of the vendatron
        '''
        command = k.command
        if self.state == self.INIT:
            
            # Begin at idle state
            self.state = self.IDLE
            self.display("Initialized")
            
            # Set up time at which screen will change
            self.curr_time = time.time()
            self.next_time = self.curr_time + 5
            
        elif self.state == self.IDLE:
            self.curr_time = time.time()
            # Get coin type if coin is inserted
            try: coin = int(command)
            except: coin = -1
            # User wants beverage, but has no funds
            if command in self.SHORTDRINK:
                self.display('Insufficient Funds')
                self.next_time = self.curr_time + 5
            # User ejects, but has not funds
            elif command == 'e':
                self.display('$0.00')
                self.next_time = self.curr_time + 5
            # User inserts coins and the machine starts keeping track of the balance
            elif (coin >= 0) & (coin < len(self.MONEY)):
                self.state = self.COUNT
                self.balance += self.MONEY[coin]
                self.display('${:0.2f}'.format(self.balance))
            # After 5 idle seconds, the screen displays 'Try Cuke Today'
            elif self.next_time != None:
                if self.curr_time >= self.next_time:
                    self.next_time = None
                    self.display('Try Cuke Today')
                
        elif self.state == self.COUNT:
            # Get coin type if coin is inserted
            try: coin = int(command)
            except: coin = -1
            
            # User selects a beverage
            if command in self.SHORTDRINK:
                price = self.DRINKS[self.SHORTDRINK[command]]
                # User does not have enough money, so transaction fails
                if round(self.balance,2) < round(price,2):
                    self.display('${:0.2f}'.format(self.balance)+'\nInsufficient Funds')
                # User has exactly enough money, so transaction occurs and the machine becomes idle
                elif round(self.balance,2) == round(price,2):
                    self.balance = 0
                    self.state = self.IDLE
                    self.curr_time = time.time()
                    self.next_time = self.curr_time + 5
                    self.display('${:0.2f}'.format(self.balance)+'\n'+self.SHORTDRINK[command]+' Released')
                # User has more than enough money, so transaction occurrs
                else:
                    self.balance -= price
                    self.display('${:0.2f}'.format(self.balance)+'\n'+self.SHORTDRINK[command]+' Released')
            
            # User ejects coins and the machine becomes idle
            elif command == 'e':
                change = getChange(round(self.balance,2))
                string = ''
                zerospots = []
                for i in range(len(change)):
                    if int(change[i]) != 0:
                        zerospots += [i]
                if len(zerospots) == 1:
                    string += str(int(change[zerospots[0]])) + ' ' + self.MONEY_LABEL[zerospots[0]] + ' '
                elif len(zerospots) == 2:
                    string += str(int(change[zerospots[0]])) + ' ' + self.MONEY_LABEL[zerospots[0]] + ' and '
                    string += str(int(change[zerospots[1]])) + ' ' + self.MONEY_LABEL[zerospots[1]] + ' '
                else:
                    for i in range(len(zerospots)):
                        string += str(int(change[zerospots[i]])) + ' ' + self.MONEY_LABEL[zerospots[i]] + ', '
                        if i == len(zerospots)-1:
                            string += 'and '
                string += 'returned.'
                self.balance = 0
                self.display('$0.00\n'+string)
                self.state = self.IDLE
                self.curr_time = time.time()
                self.next_time = self.curr_time + 5
                
            # User inserts coins
            elif (coin >= 0) & (coin < len(self.MONEY)):
                self.balance += self.MONEY[coin]             
                self.display('${:0.2f}'.format(self.balance))
                
            
    def display(self,string):
        '''
        @brief  Displays user interface and display
        @param  string  The string for the display to output
        '''
        
        print('\n________________________________________________\n')
        print('    Select Drinks:\n')
        for drink in self.DRINKS:
            print(drink+': ${:0.2f}'.format(self.DRINKS[drink])+'    ('+self.DRINKSHORT[drink]+')')
        
        print('\n    Insert Coins:\n')
        
        for i in range(len(self.MONEY)):
            print(self.MONEY_LABEL[i]+':    ('+str(i)+')')
        
        print('\nEject:    (e)\n')
        
        print('\n    Display Screen:\n')
        print(string)
        print('\n________________________________________________\n')
        
    
    
# Test code
if __name__ == "__main__":
    '''
    @brief  This runs a test file for the vendatron
    '''
    ## Vending machine instance
    v = vendatron()
    # Tester function
    while True:
        k.get_command()
        v.run()
        k.kill_command()
        
        
    